const marsGeojson = [
    // Past
    { "type": "Feature", "properties": { "name": "Viking 1 lander", class: "lander past" }, "geometry": { "type": "Point", "coordinates": [ -47.95, 22.27 ] } },
    { "type": "Feature", "properties": { "name": "Viking 2 lander", class: "lander past" }, "geometry": { "type": "Point", "coordinates": [ 134.29, 47.64 ] } },
    { "type": "Feature", "properties": { "name": "Mars Pathfinder Lander", class: "lander past" }, "geometry": { "type": "Point", "coordinates": [ -33.2, 19.1 ] } },
    { "type": "Feature", "properties": { "name": "Spirit rover (MER-A)", class: "rover past" }, "geometry": { "type": "Point", "coordinates": [ -175.47, 14.57 ] } },
    { "type": "Feature", "properties": { "name": "Opportunity rover (MER-B)", class: "rover past" }, "geometry": { "type": "Point", "coordinates": [ -5.5266, -1.9462 ] } },
    { "type": "Feature", "properties": { "name": "Phoenix Mars Lander", class: "lander past" }, "geometry": { "type": "Point", "coordinates": [ -125.7, 68.22 ] } },

    // Present
    {type: 'Feature', properties: {name: 'Curiosity rover', class: "rover active"}, geometry: {type: 'Point', coordinates: [137.4417, -4.5895]}},
    {type: 'Feature', properties: {name: 'Insight lander', class: "lander active"}, geometry: {type: 'Point', coordinates: [135.6234, 4.5024]}},

    // Future
    {type: 'Feature', properties: {name: 'Perseverance rover', class: "rover future" }, geometry: {type: 'Point', coordinates: [77.58, 18.38]}},
    {type: 'Feature', properties: {name: 'Exomars 2022 rover', class: "rover future" }, geometry: {type: 'Point', coordinates: [335.368, 18.275]}},

    // stationary points
    {type: 'Feature', properties: {name: 'Stable East Geostationary Satellite', class: 'stable satellite'}, geometry: {type: 'Point', coordinates: [167.83, 0]}},
    {type: 'Feature', properties: {name: 'Stable West Geostationary Satellite', class: 'stable satellite'}, geometry: {type: 'Point', coordinates: [-17.92, 0]}},
    {type: 'Feature', properties: {name: 'Unstable East Geostationary Satellite', class: 'unstable satellite'}, geometry: {type: 'Point', coordinates: [75.34, 0]}},
    {type: 'Feature', properties: {name: 'Unstable West Geostationary Satellite', class: 'unstable satellite'}, geometry: {type: 'Point', coordinates: [-105.55, 0]}},

    // aspirational
    // spacex data from https://www.uahirise.org/results.php?keyword=spacex&order=release_date&submit=Search
    {type: 'FeatureCollection', properties: {name: 'SpaceX candiate landing sites', class: 'spacex landing-zone'}, features: [[40, 203], [39.8, 192.1], [39.1, 196.7], [39.1, 189.8], [35.5, 163.6], [39.8, 195.6], [38.6, 190.2], [39.2, 195.6], [39.3, 199.3]].map(coord => {
        return { type: 'Feature', properties: {name: 'SpaceX candidate landing site', class: 'spacex landing-zone'}, geometry: {type: 'Point', coordinates: [coord[1], coord[0]]}}
    })}
];

const moonGeojson = [
    { type: 'Feature', properties: {name: 'Shackleton Crater Rim', class: 'locale'}, geometry: {type: 'Point', coordinates: [0, -89.9]} },
    { type: 'Feature', properties: {name: 'Malapert Mountain', class: 'locale'}, geometry: {type: 'Point', coordinates: [0, -86]} },
    { type: 'Feature', properties: {name: 'LCROSS impact site', class: 'past'}, geometry: {type: 'Point', coordinates: [-49.61, -84.72]} }
]

const circle = []
for (var i = 0; i < 360; i++) circle.push(i * Math.PI / 180)

// var coverageZones = {
//     type: 'FeatureCollection',
//     features: [
//         unstableGeo.features[0],
//         unstableGeo.features[1],
//         stableGeo.features[0],
//         stableGeo.features[1] ].map(sat => {
//         return {
//             type: 'Feature',
//             geometry: {
//                 type: 'Polygon',
//                 coordinates: [ circle.map(a => {
//                     return [
//                         sat.geometry.coordinates[0] + 80.43 * Math.sin(a),
//                         80.48 * Math.cos(a)
//                     ]
//                 }) ]
//             }
//         }
//     })
// }


// Projection Hash:
const projections = {
    mars1: (w, h) => d3.geoMercator().translate([w/2, h/2]).scale(w / (2 * Math.PI)),
    mars2: (w, h) => d3.geoMercator().rotate([180, 0, 0]).translate([w/2, h/2]).scale(w / (2 * Math.PI)),
    mars3: (w, h) => d3.geoMercator().rotate([180, 0, 0]).translate([w/2, h/2]).scale(w / (2 * Math.PI)),
    moon1: (w, h) => {
        const CLIP_ANGLE = 35 // only shows 35 degrees of the map, from -90 to -55
        const SCALE = 0.5 * (1 + Math.cos(CLIP_ANGLE * Math.PI / 180)) / Math.sin(CLIP_ANGLE * Math.PI / 180) // this took me way to long to figure out
        var projection = d3.geoStereographic()
            .clipAngle(CLIP_ANGLE)
            .rotate([0, 90])
            .scale(w * SCALE)
            .translate([w/2, h/2])
        return projection
    },
    moon2: (w, h) => {
        const CLIP_ANGLE = 35 // only shows 35 degrees of the map, from -90 to -55
        const ORIGINAL_WIDTH = 4267 // the original width of the full png from [0, -55] to [180, -55]
        const CROPPED_WIDTH = 1920 // then i cropped it just to this value without scaling anything
        const SCALE = ORIGINAL_WIDTH / 2 * (1 + Math.cos(CLIP_ANGLE * Math.PI / 180)) / Math.sin(CLIP_ANGLE * Math.PI / 180) // this took me way to long to figure out
        var browserResizedScale = w / CROPPED_WIDTH * SCALE
        var projection = d3.geoStereographic()
            .clipAngle(CLIP_ANGLE)
            .rotate([0, 90])
            .scale(browserResizedScale)
            .translate([w/2, h/2])
        return projection
    }
}

// Redraws a map for the given id
function redraw(id) {
    const $map = document.querySelector(`#${id} .basemap`)
    const bounds = $map.getBoundingClientRect()
    const w = bounds.width
    const h = bounds.height
    const projection = projections[id](w, h)
    var path = d3.geoPath().projection(projection)
    var graticulesGeoJson = d3.geoGraticule()

    const svg = d3.select(`#${id} .layer svg`)
    svg.selectAll('*').remove() // for resizing
    svg.attr('width', w + 'px').attr('height', h + 'px')
    
    // possibly add contours
    if ($map.className.includes('contours')) {
        // fetch('/contours-10-geojson.json').then(r => r.json()).then(contourGeojson => {
        //     svg.append('path')
        //         .datum(contourGeojson)
        //         .attr('class', 'contour')
        //         .attr('d', path)
        // })
    }

    svg.append("path")
        .datum(graticulesGeoJson)
        .attr('class', 'graticule')
        .attr('d', path)
    
    svg.on('mousedown.log', function() {
        console.log(projection.invert(d3.mouse(this)))
    })

    var geojson = id.includes('mars') ? marsGeojson : moonGeojson;

    geojson.map(f => {
        svg.append("path")
            .datum(f)
            .attr('class', f.properties.class)
            .attr('d', path)
            .on('mouseover', (f) => {
                console.log(`mouseover ${f.properties.name}`)
            })
    })
}

function redrawAll() {
    for (id in projections) {
        redraw(id)
    }
}

document.onreadystatechange = function(e) {
    if (document.readyState === 'complete') {
        redrawAll()
    }
}

window.onresize = function() {
    redrawAll()
}