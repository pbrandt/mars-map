# Example on how to make a mars map with d3
![screenshot](screenshot.png)

Green filled dots are active (Curiosity and Insight), cyan upcoming (nasa mars 2020/perserverence, exomars 2022), red inactive, black spacex possible sites. The white dots with the green outlines are the ground projection of the stable areostationary points, and yellow outlines are unstable.

![another screenshot](screenshot-mars2.png)

Basemap adjusted 180 degrees and turned into an svg with inkscape.

```bash
npm install
npm start
```

I just yanked the mars map png from the [USGS contour map pdf thingy](https://pubs.usgs.gov/imap/i2782/i2782_sh2.pdf). Like i literally just opened the pdf, right clicked the image, and saved it. Then i went into gimp and scaled it and turned it into a jpeg because DAMN it's a big image. here's the info.

```
mercator projection betwen +/- 57 latitudes
equatorial radius is 3396.19 km
polar radius is 3376.2 km
```

Here's the most important part of the code. These are all factories that return a d3 projection givn the map's width and height, so you can be all responsive and fancy.

```js
// Projection Factory Hash:
const projections = {
    mars1: (w, h) => d3.geoMercator().translate([w/2, h/2]).scale(w / (2 * Math.PI)),
    mars2: (w, h) => d3.geoMercator().rotate([180, 0, 0]).translate([w/2, h/2]).scale(w / (2 * Math.PI)),
    mars3: (w, h) => d3.geoMercator().rotate([180, 0, 0]).translate([w/2, h/2]).scale(w / (2 * Math.PI)),
    moon1: (w, h) => {
        const CLIP_ANGLE = 35 // only shows 35 degrees of the map, from -90 to -55
        const SCALE = 0.5 * (1 + Math.cos(CLIP_ANGLE * Math.PI / 180)) / Math.sin(CLIP_ANGLE * Math.PI / 180) // this took me way to long to figure out
        var projection = d3.geoStereographic()
            .clipAngle(CLIP_ANGLE)
            .rotate([0, 90])
            .scale(w * SCALE)
            .translate([w/2, h/2])
        return projection
    },
    moon2: (w, h) => {
        const CLIP_ANGLE = 35 // only shows 35 degrees of the map, from -90 to -55
        const ORIGINAL_WIDTH = 4267 // the original width of the full png from [0, -55] to [180, -55]
        const CROPPED_WIDTH = 1920 // then i cropped it just to this value without scaling anything
        const SCALE = ORIGINAL_WIDTH / 2 * (1 + Math.cos(CLIP_ANGLE * Math.PI / 180)) / Math.sin(CLIP_ANGLE * Math.PI / 180) // this took me way to long to figure out
        var browserResizedScale = w / CROPPED_WIDTH * SCALE
        var projection = d3.geoStereographic()
            .clipAngle(CLIP_ANGLE)
            .rotate([0, 90])
            .scale(browserResizedScale)
            .translate([w/2, h/2])
        return projection
    }
}
```

to-do

- [ ] make labels for the things on the map
- [ ] add mouseover/touch/tap effects
- [ ] better styling of the points


## contour geojson

no results yet.

using [gdal_contour](https://gdal.org/programs/gdal_contour.html)

> Install notes: i used the latest gdal, built myself and downloaded from here: [https://gdal.org/download.html#current-releases](https://gdal.org/download.html#current-releases). I also had to build latest proj 6 (not 7) from [https://proj.org/download.html](https://proj.org/download.html). Make sure you do not have libproj debian package installed, it will conflict.

- [ ] make 2d mercator projection contour plot
- [ ] make 2d stereographic projection
- [ ] give the 2d stereographic plot the illusion of being 3d that you can spin by clicking and dragging like the plotly earth one. or try hacking the plotly earth one to use mars contours instead.


# moon stuff

![moon screenshot](screenshot-moon.png)

The dots are shackleton crater, malapert mountain, and the lcros impact site.

https://pubs.usgs.gov/sim/3316/

i used the south pole. it's a stereographic projection that spans 35 degrees, from -90 to -55 latitude. D3-geo has a sterographic projection tool. Figuring out the scale was really hard, and i'm not sure i really understand what i did, but it looks right.


## cropped moon

for the scale here, since i just took a cropped segment from the orignal and the orginal is 4257 px wide, and 35 degree cop angle. i use those to calculate the scale. then *scale* the scale by the ratio of the cropped image to how big it actually is on the screen, sorry if that's confusing.

ideas: make a new moon basemap just for the south pole -90 to -80 degrees latitude. up points to 0 degrees latitude towards the earth. shadows are generated as if the sun were at -90 degrees longitude, which is not really "west" like other maps but to the left of the map.



**random links**

- https://pubs.usgs.gov/imap/i2782/i2782_sh2.pdf
- ftp://pdsimage2.wr.usgs.gov/pub/pigpen/mars/mola/contours_shapefiles/
- https://mapshaper.org/
- https://blog.mastermaps.com/2012/07/creating-contour-lines-with-gdal-and.html
- https://gist.githubusercontent.com/burritojustice/2d967b31c3bd11165b4439ebe98ecb01/raw/178f88267a0c0a83ea84f4ca30acf32cfe452f18/mars_landers.geojson
- https://observablehq.com/@d3/geotiff-contours-ii?collection=@d3/d3-geo-projection
- https://sites.google.com/a/euhsd.org/mission-to-mars/home/links/topographic-maps
- http://chrisherwig.org/hirise/Data%20Processing/2012/10/21/mars-map-projections/
- https://astrogeology.usgs.gov/search/map/Mars/GlobalSurveyor/MOLA/Mars_MGS_MOLA_DEM_mosaic_global_463m
- https://stackoverflow.com/a/14124340
- https://hi.stamen.com/how-to-make-3d-maps-of-mars-25e22914d340
- https://observablehq.com/@d3/three-axis-rotation
- https://github.com/d3/d3-geo/blob/master/README.md#projection_clipAngle
